import React, {useCallback, useEffect, useState} from 'react'
import Form from "./Form/Form";
import {useForm} from "./Form/useForm";
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import styles from './App.module.css'

const App = () => {
    const [monuments, setMonuments] = useState([{opa: 'opa'}])
    const [cemeteries, setCemeteries] = useState([])
    const {formConstructor} = useForm()

    const submit = (data) => {
        axios.post('', {data})
            .then(({data}) => {
                toast.success(data.message)
                setMonuments([])
            })
            .catch(({response}) => {
                toast.error(response.data.error)
            })
    }

    const getMonuments = useCallback((schema) => {
        axios.post('', {schema})
            .then(({data}) => {
                toast.success(data.message)
                setMonuments([])
            })
            .catch(({response}) => {
                toast.error(response.data.error)
            })
    }, [cemeteries])

    useEffect(() => {
        axios.get('')
            .then(({data}) => {
                toast.success(data.message)
                setCemeteries([])
            })
            .catch(({response}) => {
                toast.error(response.data.error)
            })
    })

    return (
        <div className={styles.main}>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <select onChange={(e) => getMonuments(e.target.value)}>
                {cemeteries.map(cemetery =>
                    <option value={cemetery.schema}>
                        {cemetery.name}
                    </option>
                )}
            </select>
            {monuments.map(monument =>
                <Form submit={submit} formConstructor={formConstructor} monument={monument}/>
            )}
        </div>
    );
}

export default App;
