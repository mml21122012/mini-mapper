import React from 'react';
import styles from "../App.module.css";
import img from "../111.jpg";

const Form = ({formConstructor, monument}) => {


    return (
        <div className={styles.form}>
            <img src={img} alt=""/>
            <div className={styles.info}>
                <div className={styles.buttons}>
                    <button>Предыдущая могила</button>
                    <button>Следующая могила</button>
                </div>
                {formConstructor.map(field =>
                    <div className={styles.field}>
                        <label>{field.label}</label>
                        <input type={field.input.type} placeholder={field.input.placeholder}/>
                    </div>
                )}
                <button className={styles.button}>Сохранить</button>
            </div>
        </div>
    );
};

export default Form;