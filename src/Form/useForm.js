
export const useForm = () => {
    const formConstructor = [
        {
            label: 'Фамилия',
            input: {
                name: 'lastname',
                placeholder: 'Введите фамилию',
                type: 'text'
            }
        },
        {
            label: 'Имя',
            input: {
                name: 'firstname',
                placeholder: 'Введите имя',
                type: 'text'
            }
        },
        {
            label: 'Отчество',
            input: {
                name: 'patronymic',
                placeholder: 'Введите Отчество',
                type: 'text'
            }
        },
        {
            label: 'Дата смерти',
            input: {
                name: 'dod',
                type: 'date'
            }
        },
        {
            label: 'Дата рождения',
            input: {
                name: 'dob',
                type: 'date'
            }
        }
    ]

    return {
        formConstructor
    }
}